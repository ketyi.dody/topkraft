const $ = require('jquery');

function onScroll() {
    function init() {
        checkNav();
        $(document).on("scroll", function() {
            if (!window.navClicked) {
                checkNav();
            }
        });

        function checkNav() {
            var navHeight = $('nav').height();
            var scrollPos = $(document).scrollTop() + navHeight;
            $('nav a.navigation').each(function () {
                var currLink = $(this);
                var refElement = $(currLink.attr("href"));
                if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                    $('#menu-center ul li a').removeClass("active");
                    currLink.addClass("active");
                } else {
                    currLink.removeClass("active");
                }
            });
        }
    }

    return {
        init,
    }
}

module.exports = onScroll;
