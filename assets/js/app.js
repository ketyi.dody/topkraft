/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

require('../css/app.css');
require('../css/cssmap-europe.css');
require('webpack-icons-installer');

const $ = require('jquery');
global.jQuery = require('jquery');
require('bootstrap');

// const videoToggle = require('./video-toggle')();
// const videoBoxHover = require('./videoBoxHover')();
// const locationPickerInit = require('./locationPickerInit')();
const smoothScroll = require('./smoothScroll')();
const onScroll = require('./onScroll')();
const imageSlider = require('./imageSlider')();
const imageMap = require('./imageMap')();
const mapHilight = require('./mapHilightInit')();
// const cssmap = require('./jquery.cssmap.min')();
// const cssMapinit = require('./cssMapInit')();

const videoBoxSelector = '.video_box';
const contactCountryCardSelector = '.contact-country-card';

$(document).ready(() => {
    window.navClicked = false;
    // $(videoBoxSelector).on('click', function() {
    //     videoToggle.toggle(this);
    // });

    // $(contactCountryCardSelector).hover(function() {
    //     cssMapinit.mousein($(this));
    // }, function() {
    //     cssMapinit.mouseout($(this));
    // });

    // videoBoxHover.hover($('.video_box'));
    // locationPickerInit.initMap();
    smoothScroll.init();
    imageSlider.init();
    imageMap.hover();
    onScroll.init();

    // mapHilight.init()
});
