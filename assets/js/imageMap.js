const $ = require('jquery');
require('image-map');
require('maphilight');

const usemap = $('img[usemap]');

function imageMap() {
    function hover() {
        usemap.imageMap();
        usemap.maphilight({
            fillColor: '008800'
        });
        $('area').click(function(e) {
            e.preventDefault();

            var mapDiv = $('div.map-europe-img.active');
            var src = mapDiv.css('background-image');
            var clicked = $(this).attr('title');
            src = src.substr(0, src.indexOf('europe/') + 7) + clicked + '.png")';
            mapDiv.css('background-image', src);

            $('.address').each(function() {
                $(this).removeClass('hidden');
                $(this).addClass('hidden');
            });
            $('#contact-' + clicked).removeClass('hidden');
        });
    }

    return {
        hover,
    }
}

module.exports = imageMap;
