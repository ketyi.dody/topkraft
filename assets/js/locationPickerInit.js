// const LocationPicker = require("location-picker");
//
// function locationPickerInit() {
//     function init() {
//         var locationPickerInit = new LocationPicker.constructor('map', {
//                 location: {
//                     latitude: "48.762080",
//                     longitude: "17.577747"
//                 }
//             }, {
//                 zoom: 6,
//             }
//         );
//     }
//
//     return {
//         init,
//     };
// }
//
// module.exports = locationPickerInit;


// const loadGoogleMapsApi = require('load-google-maps-api');

function locationPickerInit() {
    function initMap() {
        var latLong = {lat: 48.761822,lng: 17.577740};
        var map = new google.maps.Map(document.querySelector('#map'), {
            center: latLong,
            zoom: 17,
        });

        var marker = new google.maps.Marker({
            position: latLong,
            map: map,
            title: 'TopKraft s.r.o.'
        });
    }

    return {
        initMap,
    }
}

module.exports = locationPickerInit;
