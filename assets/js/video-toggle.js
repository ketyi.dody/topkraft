const $ = require('jquery');

function videoToggle() {
    function toggle(videoBox) {
        var video = $(videoBox).find('video');
        if (video.get(0).paused) {
            video.get(0).play();
            $('#play').hide();
            $('#pause').show();
        } else {
            video.get(0).pause();
            $('#pause').hide();
            $('#play').show();
        }
    }

    return {
        toggle,
    };
}

module.exports = videoToggle;
