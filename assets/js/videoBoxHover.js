const $ = require('jquery');

function videoBoxHover() {
    function hover(videoBox) {
        $(videoBox).hover(function() {
            $(this).find('img').each(function() {
                $(this).addClass('hover');
            })
        });
        $(videoBox).mouseout(function() {
            $(this).find('img').each(function() {
                $(this).removeClass('hover');
            });
            if ($('#promo_video-js').get(0).paused) {
                $(this).find('img#play').addClass('hover');
            }
        });
    }

    return {
        hover,
    }
}

module.exports = videoBoxHover;
