const $ = require('jquery');

function smoothScroll() {
    function init() {
        $('a.navigation').on('click', function (e) {
            if ($(this).hasClass('locales')) {
                return;
            }
            window.navClicked = true;
            e.preventDefault();
            $('input#toggle').prop('checked', false);
            $('html, body').animate({scrollTop: $($(this).attr('href')).offset().top - 50}, 500, 'linear', function() {
                window.navClicked = false;
            });
            $('a.navigation').each(function() {
                $(this).removeClass('active');
            });
            $(this).addClass('active');
        });
    }

    return {
        init,
    }
}

module.exports = smoothScroll;
