const $ = require('jquery');
// require('./jquery.cssmap.min');

function hover() {
    function mousein(tis) {
        var country = $(tis).attr('id');
        $('.map-europe-img').each(function() {
            $(this).removeClass('active');
        });
        $('#europe-' + country).addClass('active');
    }

    function mouseout(tis) {
        $('.map-europe-img').each(function() {
            $(this).removeClass('active');
        });
        $('#europe-default').addClass('active');
    }

    return {
        mousein,
        mouseout,
    };
}

module.exports = hover;
