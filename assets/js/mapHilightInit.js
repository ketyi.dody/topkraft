const $ = require('jquery');
require('maphilight');

function mapHilight() {
    function init() {
        $('area').on('click', function() {
            console.log(this);
            $(this).maphilight();
        });
    }

    return {
        init,
    }
}

module.exports = mapHilight;
