const $ = require('jquery');
require('slick-carousel');
const icons = require('glyphicons');

function imageSlide() {
    function init() {
        $('.responsive').slick({
            dots: false,
            prevArrow: '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><</span>',
            nextArrow: '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true">></span>',
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 2,
            adaptiveHeight: true,
            accessibility: true,
            swipe: true,
            touchMove: true,
            lazyLoad: 'ondemand',
            variableWidth: true,
            responsive: [
                {
                    breakpoint: 1921,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 2,
                        infinite: true,
                        dots: false,
                        centerMode: true,
                        centerPadding: '60px'
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

    }

    return {
        init,
    }
}

module.exports = imageSlide;
