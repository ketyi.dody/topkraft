<?php
namespace App\Controller;

use App\Entity\Catalog;
use App\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class HomepageController extends AbstractController
{
    public function indexAction(Request $request)
    {
        $catalog = $this->getDoctrine()->getRepository(Catalog::class)->findOneBy(['active' => true]);
        $contacts = $this->getDoctrine()->getRepository(Contact::class)->findByActive(true);
        $sortedContacts = [];
        /** @var Contact $contact */
        foreach ($contacts as $contact) {
            $sortedContacts[$contact->getCountry()] = $contact;
        }
        return $this->render('homepage/index.html.twig', [
            'catalog' => $catalog,
            'locale' => $request->getSession()->get('_locale'),
            'contacts' => $sortedContacts,
        ]);
    }
}
