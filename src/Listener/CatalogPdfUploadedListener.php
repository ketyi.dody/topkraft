<?php
namespace App\Listener;

use App\Entity\Catalog;
use App\Service\PdfToImageService;
use Doctrine\ORM\Event\LifecycleEventArgs;

class CatalogPdfUploadedListener
{
    private $pdfToImageService;

    public function __construct(PdfToImageService $pdfToImageService)
    {
        $this->pdfToImageService = $pdfToImageService;
    }

    public function postPersist(Catalog $catalog, LifecycleEventArgs $args)
    {
        $catalog = $this->pdfToImageService->processPdf($catalog);
        $args->getEntityManager()->persist($catalog);
        $args->getEntityManager()->flush();
    }
}
