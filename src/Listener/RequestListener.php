<?php

namespace App\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        // some logic to determine the $locale
        $locale = $request->get('_locale') ?? 'sk';
        $request->setLocale($locale);
    }
}