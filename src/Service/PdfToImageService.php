<?php
namespace App\Service;

use App\Entity\Catalog;
use App\Entity\CatalogImage;
use Imagick;
use Spatie\PdfToImage\Exceptions\PdfDoesNotExist;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Finder\Finder;

class PdfToImageService
{
    private $fileUploadDir = './uploads/images/pdf';

    private $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function processPdf(Catalog $catalog)
    {

        // @TODO transmission this to PdfToImage from spatie/pdf-to-image
        // convert pdf to images
        $pdfFile = $catalog->getCatalogFile()->getPathname();

//        try {
//            $pdf = new Imagick($catalog->getCatalogFile()->getPathname());
//            dump($pdf->getNumberImages());
//            dump($pdf);
//            die;
//        } catch (\Exception $exception) {
//            die($exception->getMessage());
//        }
//
//        try {
//            $pdfFile = new \Spatie\PdfToImage\Pdf($catalog->getCatalogFile()->getPathname());
//        } catch (PdfDoesNotExist $exception) {
//            die($exception->getMessage());
//        } catch (\Exception $exception) {
//            die($exception->getMessage());
//        }
//
//        for ($i = 1; $i <= $pdfFile->getNumberOfPages(); $i++) {
//            $pdfFile->setPage($i)
//                ->setOutputFormat('png')
//                ->setCompressionQuality(100)
//                ->saveImage($this->fileUploadDir . '/catalog-' . $catalog->getId() . '.png');
//        }

        $command = 'gs -o ' . $this->fileUploadDir . '/catalog-' . $catalog->getId() . '-%03d.png -sDEVICE=png16m -r144 ' . $pdfFile;

        exec($command, $output, $return_var);

        if ($return_var != 0) {
            return $catalog;
        }

        // process pdf images
        $catalog = $this->processImages($catalog);

        return $catalog;
    }

    private function processImages(Catalog $catalog)
    {

        $finder = new Finder();
        $finder->files()->in(array('folder' => $this->fileUploadDir));
        $finder->sort(function ($a, $b) { return strcmp($a->getRealpath(), $b->getRealpath()); });

        foreach ($finder as $file) {
            if (strpos($file->getFilename(), 'catalog-' . $catalog->getId()) === false) {
                continue;
            }
            $catalogImage = new CatalogImage();
            $file = new File($file->getRealPath());
            $catalogImage->setImageFile($file);
            $catalogImage->setActive(true);
            $catalogImage->setCatalog($catalog);

            $catalog->addCatalogImage($catalogImage);
        }

        return $catalog;
    }

}
