<?php

namespace App\Entity;

use App\Entity\Traits\BlameableTrait;
use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class Catalog
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\CatalogRepository")
 * @ORM\Table(name="catalog")
 * @Vich\Uploadable
 */
class Catalog
{
    use BlameableTrait;

    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active = false;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="CatalogCategory")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $catalog;

    /**
     * @Vich\UploadableField(mapping="catalogs", fileNameProperty="catalog")
     * @var File
     */
    private $catalogFile;

    /**
     * @ORM\OneToMany(targetEntity="CatalogImage", mappedBy="catalog", cascade={"persist", "remove"})
     */
    private $catalogImages;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getCatalog()
    {
        return $this->catalog;
    }

    /**
     * @param string $catalog
     */
    public function setCatalog(string $catalog): void
    {
        $this->catalog = $catalog;
    }

    /**
     * @return File
     */
    public function getCatalogFile(): ?File
    {
        return $this->catalogFile;
    }

    /**
     * @param File $catalogFile
     */
    public function setCatalogFile(File $catalogFile): void
    {
        $this->catalogFile = $catalogFile;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getCatalogImages()
    {
        return $this->catalogImages;
    }

    public function setCatalogImages($catalogImages): void
    {
        $this->catalogImages = $catalogImages;
    }

    public function addCatalogImage(CatalogImage $catalogImage): void
    {
        $this->catalogImages[] = $catalogImage;
    }
}
