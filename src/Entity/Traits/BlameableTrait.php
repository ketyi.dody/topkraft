<?php
namespace App\Entity\Traits;

use Gedmo\Mapping\Annotation\Blameable;
use Doctrine\ORM\Mapping as ORM;

trait BlameableTrait
{
    /**
     * @Blameable(on="create")
     * @ORM\Column(type="string")
     */
    protected $createdBy;

    /**
     * @var string $updatedBy
     *
     * @Blameable(on="update")
     * @ORM\Column(type="string")
     */
    private $updatedBy;

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy ?? 'Anonymous';
        return $this;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(string $updatedBy)
    {
        $this->updatedBy = $updatedBy ?? 'Anonymous';
        return $this;
    }
}
