<?php

namespace App\Entity;

use App\Entity\Traits\BlameableTrait;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Contact
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 * @ORM\Table(name="contact")
 */
class Contact
{
    use BlameableTrait;

    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active = false;

    /**
     * @ORM\Column(type="text")
     */
    protected $country;

    /**
     * @ORM\Column(type="text")
     */
    protected $companyName;

    /**
     * @ORM\Column(type="text")
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(type="text")
     */
    protected $tollFreeNumber;

    /**
     * @ORM\Column(type="text")
     */
    protected $faxNumber;

    /**
     * @ORM\Column(type="text")
     */
    protected $email;

    /**
     * @ORM\Column(type="text")
     */
    protected $businessId;

    /**
     * @ORM\Column(type="text")
     */
    protected $taxId;

    /**
     * @ORM\Column(type="text")
     */
    protected $vatRegistrationNumber;

    /**
     * @ORM\Column(type="text")
     */
    protected $city;

    /**
     * @ORM\Column(type="text")
     */
    protected $street;

    /**
     * @ORM\Column(type="text")
     */
    protected $number;

    /**
     * @ORM\Column(type="text")
     */
    protected $zip;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function setCompanyName($companyName): void
    {
        $this->companyName = $companyName;
    }

    public function getTollFreeNumber()
    {
        return $this->tollFreeNumber;
    }

    public function setTollFreeNumber($tollFreeNumber): void
    {
        $this->tollFreeNumber = $tollFreeNumber;
    }

    public function getFaxNumber()
    {
        return $this->faxNumber;
    }

    public function setFaxNumber($faxNumber): void
    {
        $this->faxNumber = $faxNumber;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getBusinessId()
    {
        return $this->businessId;
    }

    public function setBusinessId($businessId): void
    {
        $this->businessId = $businessId;
    }

    public function getTaxId()
    {
        return $this->taxId;
    }

    public function setTaxId($taxId): void
    {
        $this->taxId = $taxId;
    }

    public function getVatRegistrationNumber()
    {
        return $this->vatRegistrationNumber;
    }

    public function setVatRegistrationNumber($vatRegistrationNumber): void
    {
        $this->vatRegistrationNumber = $vatRegistrationNumber;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city): void
    {
        $this->city = $city;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function setStreet($street): void
    {
        $this->street = $street;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number): void
    {
        $this->number = $number;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function setZip($zip): void
    {
        $this->zip = $zip;
    }
}
