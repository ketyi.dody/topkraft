<?php
namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class CatalogImage
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="catalog_image")
 * @Vich\Uploadable
 */
class CatalogImage
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active = false;

    /**
     * @ORM\ManyToOne(targetEntity="Catalog", inversedBy="catalogImages")
     * @ORM\JoinColumn(name="catalog_id", referencedColumnName="id")
     */
    protected $catalog;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="catalogImages", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    public function __toString()
    {
        return $this->image;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return File
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File $imageFile
     * @throws \Exception
     */
    public function setImageFile(File $imageFile): void
    {
        $this->imageFile = $imageFile;
        if ($imageFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->image = $imageFile->getFilename();
            $this->updated = new \DateTime('now');
        }
    }

    public function getCatalog()
    {
        return $this->catalog;
    }

    public function setCatalog($catalog): CatalogImage
    {
        $this->catalog = $catalog;
        return $this;
    }

}
